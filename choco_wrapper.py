#!/usr/bin/env python
# Enter your code here. Read input from STDIN. Print output to STDOUT
T = int(raw_input())
for i in range (0,T):
    A,B,C1 = [int(x) for x in raw_input().split(' ')]
    answer = 0
    # write code to compute answer
    buys = A/B
    answer = buys
    wrappers = buys
    while wrappers >= C1:
        #import pdb; pdb.set_trace()
        buys = wrappers/C1
        wrappers = wrappers%C1
        answer += buys
        wrappers += buys
    print answer
