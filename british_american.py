#!/usr/bin/env python

import sys
import fileinput

def main():
    N = int(sys.stdin.readline())
    lines = ''
    for i in xrange(0,N):
        lines = lines + ' '+ sys.stdin.readline()
    words = lines.split()
    T = int(sys.stdin.readline())

    for i in xrange(0,T):
        usword = sys.stdin.readline().strip()
        ukword = usword[:-2]+'se'
        count = words.count(usword) + words.count(ukword)
        print count
if __name__ == '__main__':
    main()
