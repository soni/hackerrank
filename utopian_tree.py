#!/usr/bin/env python

import sys
import fileinput

def main():
    first = sys.stdin.readline()
    for line in fileinput.input():
        cycles = int(line)
        h = 1
        for i in xrange(1,cycles+1):
            if i%2 ==0:
                h += 1
            else:
                h *= 2
        print h

if __name__ == '__main__':
    main()
