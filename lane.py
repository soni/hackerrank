#!/usr/bin/env python

import sys
import fileinput

def main():
    first = sys.stdin.readline()
    [N, T] = first.strip().split()
    second = sys.stdin.readline()
    width = second.strip().split()
    
    for line in fileinput.input():
        [entry, ex] = line.split()
        print min(width[int(entry):int(ex)+1])

if __name__ == '__main__':
    main()
